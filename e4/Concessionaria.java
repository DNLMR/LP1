package e4;

import java.util.HashMap;
import java.util.Map;
  
public class Concessionaria {
  
       public static void main(String args[]) {
  
             Map<String, String> Concessionaria = new HashMap<String, String>();
              
             Concessionaria.put("Del Rey", new String("Azul met�lico"));
             Concessionaria.put("Gol Quadrado", new String("Preto Envolopado"));
             Concessionaria.put("Gol bola", new String("Branco"));
             Concessionaria.put("Gol G5", new String("Vermelho"));
             Concessionaria.put("Fox", new String("Prata"));
              
             for (String key : Concessionaria.keySet()) {
                    String value = Concessionaria.get(key);
                    System.out.println(key + " = " + value);
             }
             
             String key = "Ferrari";
             String value = Concessionaria.get(key);
             if (value == null) {
            	 System.out.println("N�o possuimos o carro '" + key + "' no momento");
             } else {
            	 System.out.println(key + " = " + value);
             }
       }

}