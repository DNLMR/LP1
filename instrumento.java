package e3;

public class instrumento {
	private String tipo;
	private float valor_medio;
	private String nome;

//construtor	
   public instrumento(String nome, float valor_medio){
	   this.valor_medio=valor_medio;
	   this.nome=nome;
   }
   
   //setters
    public void settipo(String tipo2){
    	tipo=tipo2;
    }
    
    //getters
    
    public float getvalor_medio(){
    	return valor_medio;
    }
    
    public String gettipo(){
    	return tipo;
    }
    
    public String getnome(){
    	return nome;
    }
}
