package e5;

public class Team {
	private int id_team;
	private int fk_league;
	private String team_name;
	
	public int getId_team() {
		return id_team;
	}
	public void setId_team(int id_team) {
		this.id_team = id_team;
	}
	public int getFk_league() {
		return fk_league;
	}
	public void setFk_league(int fk_league) {
		this.fk_league = fk_league;
	}
	public String getTeam_name() {
		return team_name;
	}
	public void setTeam_name(String team_name) {
		this.team_name = team_name;
	}
}
