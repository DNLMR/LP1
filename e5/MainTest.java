package e5;

import java.util.ArrayList;

public class MainTest {
	public static void main(String[] args) {
		DataSource ds = new DataSource();
		TeamDAO teamDao = new TeamDAO(ds);
		ArrayList<Team> lista = teamDao.readAll();
		if (lista != null) {
			for (Team t : lista) {
				System.out.println(t.getTeam_name());
			}
		}
		
		ds.closeDataSource();
	}
}
