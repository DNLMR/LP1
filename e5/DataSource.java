package e5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataSource {
	
	private String hostname;
	private int port;
	private String database;
	private String username;
	private String password;
	
	private Connection connection;
	
	public DataSource() {
		try {
			hostname = "localhost";
			port 	 = 3306;
			database = "SPORTS";
			username = "daniel_dev";
			password = "123456";
			
			String  url = "jdbc:mysql://"+hostname+":"+port+"/"+database+"?serverTimezone=UTC";
			DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
			connection= DriverManager.getConnection(url,username,password);
			System.out.println("deu certo abrir");
		}
		
		catch(SQLException ex) {
			System.err.println("Erro Conex�o "+ex.getMessage());
		}
		catch(Exception ex) {
			System.err.println("Erro Geral "+ex.getMessage());
		}
	}
	
	public Connection getConnection() {
		return  this.connection;
	}
	
	public void closeDataSource() {
		try {
			connection.close();
			System.out.println("deu certo fechar");
		}
		catch(Exception ex) {
			System.err.println("Erro  ao desconectar"+ex.getMessage());
		}
		
	}

}
