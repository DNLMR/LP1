package e5;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TeamDAO {
	private DataSource  dataSource;
	
	public TeamDAO(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public ArrayList<Team> readAll(){
		try {
			String SQL = "SELECT * FROM teams";
			PreparedStatement ps = dataSource.getConnection().prepareStatement(SQL);
			ResultSet rs = ps.executeQuery();
			
			ArrayList<Team> lista = new ArrayList<Team>();
			
			while(rs.next()) {
				Team team = new Team();
				team.setId_team(rs.getInt("id_team"));
				team.setFk_league(rs.getInt("fk_league"));
				team.setTeam_name(rs.getString("team_name"));
				lista.add(team);
			}
			
			ps.close();
			return lista;
		}
		catch(SQLException ex) {
			System.err.println("Erro no statement "+ex.getMessage());
		}
		catch(Exception ex) {
			System.err.println("Erro Geral "+ex.getMessage());
		}
		return null;
	}
}
