package e3;

public class lugar {
	private String var_cidade;
	private boolean var_corona;
	private int var_mortes;
	private String var_pais;
	
	//construtor
	public lugar(String cidade, String pais){
		var_cidade = cidade;
		var_pais = pais;
	}
	
	//setters
    public void set_corona_status(boolean corona){
    	var_corona =corona;
    }
    
    public void set_corona_mortes(int mortes){
    	var_mortes = mortes;
    }
    
	//getters
        
    public String get_cidade(){
    	return var_cidade;
    }
    
    public String get_pais(){
    	return var_pais;
    }
    
    public int get_mortes(){
    	return var_mortes;
    }
    
    public String get_status() {
    	String var_frase;
    	if (var_corona == true) {
    		var_frase = "Virus ainda n�o arradicado";
    	} else  {
    		var_frase = "Virus ja foi erradicado";
    	}
    	return var_frase;
    }
}
