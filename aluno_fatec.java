package e3;

public class aluno_fatec {
		boolean formado;
		String curso;
		String nome;
		
		//construtor
	    public aluno_fatec(String nome, boolean formado){
	    	this.nome=nome;
	    	this.formado=formado;
	    }
	    
	    //setters
	    public void set_curso(String curso){
	    	this.curso = curso;
	    }
	    
	    
	  //getters
	  public String get_curso() {
		  return curso;
	  }  
	  
	  public String  get_nome() {
		  return nome;
	  }
	  
	  public String get_status() {
	    	String var_frase;
	    	if (formado == true) {
	    		var_frase = "Este aluno j� � formado";
	    	} else  {
	    		var_frase = "Aluno ainda estudando na FATEC";
	    	}
	    	return var_frase;
	    }
		


}
